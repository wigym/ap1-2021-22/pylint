def obdobi(mesic):
    """ napoveda funkce"""
    if mesic==1 or mesic==2 or mesic==12:
        rocni_obdobi="Zima"
        if mesic==1:
            nazevm="lednu"
        elif mesic==2:
            nazevm="únoru"
        elif mesic==12:
            nazevm="prosinci"
    elif mesic==3 or mesic==4 or mesic==5:
        rocni_obdobi="Jaro"
        if mesic==3:
            nazevm="březnu"
        elif mesic==4:
            nazevm="dubnu"
        elif mesic==5:
            nazevm="květnu"
    elif mesic==6 or mesic==7 or mesic==8:
        rocni_obdobi="Léto"
        if mesic==6:
            nazevm="červnu"
        elif mesic==7:
            nazevm="červenci"
        elif mesic==8:
            nazevm="srpnu"
    elif mesic==9 or mesic==10 or mesic==11:
        rocni_obdobi="Podzim"
        if mesic==9:
            nazevm="září"
        elif mesic==10:
            nazevm="říjnu"
        elif mesic==11:
            nazevm="listopadu"
    print(f"{rocni_obdobi} je v {nazevm}.")

obdobi(5)
